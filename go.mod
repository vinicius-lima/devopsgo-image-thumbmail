module bitbucket.org/BragaDevOps/sls_devopsgo_image_thumbnail_sqs

go 1.12

require (
	github.com/aws/aws-lambda-go v1.13.0
	github.com/aws/aws-sdk-go v1.23.6
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/disintegration/imaging v1.6.1
	github.com/golang/mock v1.3.1 // indirect
	github.com/haya14busa/goverage v0.0.0-20180129164344-eec3514a20b5 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/kyoh86/richgo v0.3.3 // indirect
	github.com/kyoh86/xdg v1.0.0 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/subosito/gotenv v1.2.0
	github.com/urfave/cli v1.22.1 // indirect
	github.com/wacul/ptr v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf // indirect
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de // indirect
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20191029155521-f43be2a4598c // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20191030062658-86caa796c7ab // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
)

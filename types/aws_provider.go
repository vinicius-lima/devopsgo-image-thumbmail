package types

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"os"
	"context"
)

type AWSProvider interface {
	SetConfig(config *aws.Config)
	NewSession()
	Initialize(handler func(ctx context.Context, sqsEvent *events.SQSEvent) (int, error))
	DownloadObjectS3(file *os.File, bucket, key string) error
	UploadToS3(tmpImagePath, bucket, key string) error
}

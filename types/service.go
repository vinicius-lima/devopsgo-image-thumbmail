package types

import (
	"context"
	"github.com/aws/aws-lambda-go/events"
)

type Service interface {
	Start()
	Make(ctx context.Context, sqsEvent *events.SQSEvent) (int, error)
}